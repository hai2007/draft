import Clunch from 'clunch';

Clunch.series('ui-series', ['number', 'string', 'color', 'boolean', '$rotate', '$move', ($number, $string, $color, $boolean, $rotate, $move) => {

    return {

        attrs: {
            type: $string(),
            "is-move": $boolean(false),
            x1: $number()(true),
            y1: $number()(true),
            x2: $number()(true),
            y2: $number()(true),
            content: $string(""),
            color: $color()
        },
        region: {
            default(render, attr) {
                render().fillRect(attr.x1, attr.y1, attr.x2 - attr.x1, attr.y2 - attr.y1);
            }
        },
        link(painter, attr) {

            painter.config({
                'strokeStyle': attr.color,
                'fillStyle': attr.color,
                'textBaseline': 'top',
                'font-size': 12
            });

            switch (attr.type) {

                case "to": {

                    let temp = $move(attr.x1 - attr.x2, attr.y1 - attr.y2, 10, attr.x2, attr.y2);

                    painter.beginPath().moveTo(attr.x1, attr.y1).lineTo(attr.x2, attr.y2).stroke();

                    painter.beginPath()
                        .moveTo(...$rotate(attr.x2, attr.y2, 0.5, ...temp))
                        .lineTo(attr.x2, attr.y2)
                        .lineTo(...$rotate(attr.x2, attr.y2, -0.5, ...temp))
                        .stroke();

                    break;
                }

                case "line": {
                    painter.beginPath().moveTo(attr.x1, attr.y1).lineTo(attr.x2, attr.y2).stroke();
                    break;
                }

                case "rect": {
                    painter.strokeRect(attr.x1, attr.y1, attr.x2 - attr.x1, attr.y2 - attr.y1);
                    break;
                }

                case "text": {
                    let contents = attr.content.split('\n');
                    for (let i = 0; i < contents.length; i++) {
                        painter.fillText(contents[i], attr.x1, attr.y1 + i * 14);
                    }
                    break;
                }

            }

            // 如果需要移动，就显示可以编辑的边界
            if (attr['is-move']) {

                painter.config({
                    "strokeStyle": "pink",
                    "lineDash": [5]
                }).strokeRect(attr.x1 - 2, attr.y1 - 2, attr.x2 - attr.x1 + 4, attr.y2 - attr.y1 + 4);

                let points = [

                    [attr.x1 - 2, attr.y1 - 2],
                    [attr.x2 + 2, attr.y1 - 2],
                    [attr.x1 - 2, attr.y2 + 2],
                    [attr.x2 + 2, attr.y2 + 2],

                    [0.5 * (attr.x1 + attr.x2), attr.y1 - 2],
                    [0.5 * (attr.x1 + attr.x2), attr.y2 + 2],
                    [attr.x1 - 2, 0.5 * (attr.y1 + attr.y2)],
                    [attr.x2 + 2, 0.5 * (attr.y1 + attr.y2)]

                ];

                painter.config({
                    "fillStyle": "white",
                    "lineDash": []
                });

                for (let item of points) {
                    painter.fullCircle(item[0], item[1], 6);
                }

            }

        }

    };

}]);
